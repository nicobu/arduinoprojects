#include <Adafruit_NeoPixel.h>
#include <Servo.h>

#define LED_PIN 2
#define NUM_LEDS 1
#define LED_TOUCH_BUTTON_PIN 12
#define FLOWER_TOUCH_BUTTON_PIN 11
#define COLOR_POT_PIN 0
#define INTENSITY_POT_PIN 1

#define ENCODER_PIN_SW 5   // SW  = SWITCH -> encoder button
#define ENCODER_PIN_CLK 4  // CLK = CLOCK  -> check first
#define ENCODER_PIN_DT 3   // DT  = DATA   -> direction

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

int colorPotValue  = 0;
int intensityPotValue  = 0;

// exponential moving average for potis
float EMA_COLOR_a = 0.2;      //initialization of EMA alpha
int EMA_COLOR_S = 0;          //initialization of EMA S

float EMA_INTENSITY_a = 0.2;      //initialization of EMA alpha
int EMA_INTENSITY_S = 0;          //initialization of EMA S
int oldEMA_INTENSITY_S = 0;          //initialization of EMA S

// ******************** LEDs ********************
bool ledOn = false;
int intensity = 20;
int steps = 10;
float intFactor = (float)intensity / (float)steps;
float rgbDiv = (float)1023 / (float)(steps * 6);

int rgbValue = 0;
int oldRgbValue = 0;
int red = 0;
int grn = 0;
int blu = 0;

// ******************** buttons ********************
int ledTouchButtonValue = LOW;
int oldLedTouchButtonValue = LOW;
bool doLedSwitch = false;

int flowerTouchButtonValue = LOW;
int oldFlowerTouchButtonValue = LOW;
bool doFlowerSwitch = false;

// ******************** encoder ********************
bool doEncoderButtonAction = false;
int oldEncoderButtonValue = LOW;
int encoderValue = 0;
int encoderValue_old = 0;
int encoderPinCLK = LOW;
int encoderPinCLK_old = LOW;

// ******************** servo ********************
Servo servo;
int angle = 0;

// ******************** setup ********************
void setup() {
  Serial.begin(9600);

  strip.begin();
  strip.setPixelColor(0, 0, 0, 0);
  strip.show();

  pinMode(ENCODER_PIN_CLK, INPUT);
  pinMode(ENCODER_PIN_DT, INPUT);

  servo.attach(8);
  servo.write(angle);

  EMA_COLOR_S = analogRead(COLOR_POT_PIN);  //set EMA S for t=1
  EMA_INTENSITY_S = analogRead(INTENSITY_POT_PIN);  //set EMA S for t=1
}

// ******************** loop ********************
void loop() {
  // ******************** led touch button ********************
  ledTouchButtonValue = digitalRead(LED_TOUCH_BUTTON_PIN);
  if(ledTouchButtonValue != oldLedTouchButtonValue) {
    // touch state changed
    if(ledTouchButtonValue == HIGH) { // touch sends HIGH when pressed
      // touch button pushed
      doLedSwitch = true;
      ledOn = !ledOn;
    } else {
      // touch button released
    }
    oldLedTouchButtonValue = ledTouchButtonValue;
  }

  if(doLedSwitch) {
    if(ledOn) {
      strip.setPixelColor(0, red, grn, blu);
      strip.show();
    } else {
      strip.setPixelColor(0, 0, 0, 0);
      strip.show();
    }
    doLedSwitch = false;
  }

  if(ledOn) {
    colorPotValue = analogRead(COLOR_POT_PIN);
    EMA_COLOR_S = (EMA_COLOR_a * colorPotValue) + ((1 - EMA_COLOR_a) * EMA_COLOR_S);
    
    intensityPotValue = analogRead(INTENSITY_POT_PIN);
    EMA_INTENSITY_S = (EMA_INTENSITY_a * (intensityPotValue / 4)) + ((1 - EMA_INTENSITY_a) * EMA_INTENSITY_S);
//    EMA_INTENSITY_S /= 4; // value between 0 and 255 (int operation "rounds down")
    
//    intFactor = (float)intensity / (float)steps;
    intFactor = (float)EMA_INTENSITY_S / (float)steps;
    
//    rgbValue = (float)potValue / rgbDiv;
    rgbValue = (float)EMA_COLOR_S / rgbDiv;
    
    if(rgbValue != oldRgbValue || EMA_INTENSITY_S != oldEMA_INTENSITY_S) {
      Serial.print("colorPot: "); Serial.print(EMA_COLOR_S);
      Serial.print(" intensityPot: "); Serial.print(EMA_INTENSITY_S);
      Serial.print("    ");
  
      int tmp = 0;
  
      if(rgbValue == 0) {
        red = steps * intFactor;
        grn = 0;
        blu = 0;
      } else if(rgbValue <= steps) {
        red = steps * intFactor;
        grn = rgbValue * intFactor;
        blu = 0;
      } else if(rgbValue <= 2 * steps) {
        tmp = rgbValue - 1 * steps;
        red = (steps - tmp) * intFactor;
        grn = steps * intFactor;
        blu = 0;
      } else if(rgbValue <= 3 * steps) {
        tmp = rgbValue - 2 * steps;
        red = 0;
        grn = steps * intFactor;
        blu = tmp * intFactor;
      } else if(rgbValue <= 4 * steps) {
        tmp = rgbValue - 3 * steps;
        red = 0;
        grn = (steps - tmp) * intFactor;
        blu = steps * intFactor;
      } else if(rgbValue <= 5 * steps) {
        tmp = rgbValue - 4 * steps;
        red = tmp * intFactor;
        grn = 0;
        blu = steps * intFactor;
      } else {
        tmp = rgbValue - 5 * steps;
        red = steps * intFactor;
        grn = 0;
        blu = (steps - tmp) * intFactor;
      }

      Serial.print( "r: "); Serial.print(red);
      Serial.print(" g: "); Serial.print(grn);
      Serial.print(" b: "); Serial.println(blu);
      
      strip.setPixelColor(0, red, grn, blu);
      strip.show();
  
      oldRgbValue = rgbValue;
      oldEMA_INTENSITY_S = EMA_INTENSITY_S;
    }
  }

  // ******************** flower touch button ********************
  flowerTouchButtonValue = digitalRead(FLOWER_TOUCH_BUTTON_PIN);
  if(flowerTouchButtonValue != oldFlowerTouchButtonValue) {
    // touch state changed
    if(flowerTouchButtonValue == HIGH) { // touch sends HIGH when pressed
      // touch button pushed
      doFlowerSwitch = true;
      ledOn = !ledOn;
    } else {
      // touch button released
    }
    oldFlowerTouchButtonValue = flowerTouchButtonValue;
  }

  if(doFlowerSwitch) {
    // TODO: servo
    if(ledOn) {
      strip.setPixelColor(0, red, grn, blu);
      strip.show();
    } else {
      strip.setPixelColor(0, 0, 0, 0);
      strip.show();
    }
    doFlowerSwitch = false;
  }

  // ******************** encoder button ********************
  int encoderButtonValue = digitalRead(ENCODER_PIN_SW);
  if(encoderButtonValue != oldEncoderButtonValue) {
    // state changed
    if(encoderButtonValue == LOW) { // encoder sends LOW when pressed
      // touch button pushed
      Serial.println("button pushed");
      doEncoderButtonAction = true;
    } else {
      // touch button released
    }
    oldEncoderButtonValue = encoderButtonValue;
  }

  if(doEncoderButtonAction) {
    // button actions
    if(angle == 0) encoderValue = 170;
    else encoderValue = 0;
    
    angle = encoderValue;
    servo.write(angle);
    doEncoderButtonAction = false;
  }

  // ******************** encoder rotation ********************
  encoderPinCLK = digitalRead(ENCODER_PIN_CLK);
  if(encoderPinCLK == HIGH && encoderPinCLK_old == LOW) {
    // encoder turned
    int encoderPinDT = digitalRead(ENCODER_PIN_DT);
    if(encoderPinDT == HIGH) {
      encoderValue++;
    } else {
      encoderValue--;
    }
  }
  encoderPinCLK_old = encoderPinCLK;
  
  encoderValue = constrain(encoderValue, 0, 170);
  if(encoderValue != encoderValue_old) {
    angle = encoderValue;
    servo.write(angle);
    Serial.print("angle: "); Serial.println(angle);
  }
  encoderValue_old = encoderValue;
}
