#include <Servo.h>

#define ENCODER_PIN_SW 2   // SW  = SWITCH -> encoder button
#define ENCODER_PIN_CLK 4  // CLK = CLOCK  -> check first
#define ENCODER_PIN_DT 3   // DT  = DATA   -> direction

Servo servo;
int angle = 0;

bool doButtonAction = false;

int oldEncoderButtonValue = LOW;
int encoderValue = 0;
int encoderValue_old = 0;
int encoderPinCLK = LOW;
int encoderPinCLK_old = LOW;

void setup() {
  Serial.begin(9600);
  pinMode(ENCODER_PIN_SW, INPUT);
  pinMode(ENCODER_PIN_CLK, INPUT);
  pinMode(ENCODER_PIN_DT, INPUT);

  servo.attach(8);
  servo.write(angle);
}

void loop() {
  // ******************** encoder button ********************
  int encoderButtonValue = digitalRead(ENCODER_PIN_SW);
  if(encoderButtonValue != oldEncoderButtonValue) {
    // state changed
    if(encoderButtonValue == LOW) { // encoder sends LOW when pressed
      // touch button pushed
      Serial.println("button pushed");
      doButtonAction = true;
    } else {
      // touch button released
    }
    oldEncoderButtonValue = encoderButtonValue;
  }

  if(doButtonAction) {
    // button actions
    encoderValue = 0;
    angle = 0;
    servo.write(angle);
    doButtonAction = false;
  }

  // ******************** encoder rotation ********************
  encoderPinCLK = digitalRead(ENCODER_PIN_CLK);
  if(encoderPinCLK == HIGH && encoderPinCLK_old == LOW) {
    // encoder turned
    int encoderPinDT = digitalRead(ENCODER_PIN_DT);
    if(encoderPinDT == HIGH) {
      encoderValue++;
    } else {
      encoderValue--;
    }
  }
  encoderPinCLK_old = encoderPinCLK;
  
  encoderValue = constrain(encoderValue, 0, 180);
  if(encoderValue != encoderValue_old) {
    angle = encoderValue;
    servo.write(angle);
    Serial.print("angle: "); Serial.println(angle);
  }
  encoderValue_old = encoderValue;
}
