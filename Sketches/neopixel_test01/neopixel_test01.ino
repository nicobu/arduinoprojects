#include <Adafruit_NeoPixel.h>

#define LED_PIN 2
#define NUM_LEDS 1

#define RED_ENCODER_BUTTON_PIN 12
#define RED_ENCODER_PIN1 4
#define RED_ENCODER_PIN2 5

#define GREEN_ENCODER_PIN1 6
#define GREEN_ENCODER_PIN2 7

#define BLUE_ENCODER_PIN1 8
#define BLUE_ENCODER_PIN2 9

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

int redIntensity = 50;
int oldRedIntensity = 50;

int greenIntensity = 0;
int oldGreenIntensity = 0;

int blueIntensity = 0;
int oldBlueIntensity = 0;

int del = 25;

int oldEncoderButtonValue = LOW;
bool doSwitch = false;
bool ledOn = false;

int redEncoderValue = 0;
int redEncoderPin1 = LOW;
int redEncoderPin1_old = LOW;

int greenEncoderValue = 0;
int greenEncoderPin1 = LOW;
int greenEncoderPin1_old = LOW;

int blueEncoderValue = 0;
int blueEncoderPin1 = LOW;
int blueEncoderPin1_old = LOW;
//unsigned long lastEncoderChange = 0L;
//int encoderDiff = 0;

void setup() {
  Serial.begin(9600);
  pinMode(RED_ENCODER_BUTTON_PIN, INPUT);
  pinMode(RED_ENCODER_PIN1, INPUT);
  pinMode(RED_ENCODER_PIN2, INPUT);

  pinMode(GREEN_ENCODER_PIN1, INPUT);
  pinMode(GREEN_ENCODER_PIN2, INPUT);

  pinMode(BLUE_ENCODER_PIN1, INPUT);
  pinMode(BLUE_ENCODER_PIN2, INPUT);
  
  strip.begin();
  strip.setPixelColor(0, 0, 0, 0);
  strip.show();
}

void loop() {
//  unsigned long currentMillis = millis();
  
  int encoderButtonValue = digitalRead(RED_ENCODER_BUTTON_PIN);
  
  if(encoderButtonValue != oldEncoderButtonValue) {
    // touch state changed
    if(encoderButtonValue == HIGH) { // touch sends HIGH when pressed
      // touch button pushed
      ledOn = !ledOn;
      doSwitch = true;
    } else {
      // touch button released
    }
    oldEncoderButtonValue = encoderButtonValue;
  }
  
  if(doSwitch) { // execute once
    if(ledOn) {
      Serial.print("redIntensity: "); Serial.println(redIntensity);
      Serial.print("greenIntensity: "); Serial.println(greenIntensity);
      Serial.print("blueIntensity: "); Serial.println(blueIntensity);
      strip.setPixelColor(0, redIntensity, greenIntensity, blueIntensity);
      strip.show();
    } else {
      strip.setPixelColor(0, 0, 0, 0);
      strip.show();
    }
    doSwitch = false;
  }

//  Serial.print("ledOn: ");
//  Serial.println(ledOn);
  
  if(ledOn) {
    bool setStrip = false;
    int multi = 10;
    
    // ***************************** RED ***************************** //
    redEncoderPin1 = digitalRead(RED_ENCODER_PIN1);
    if(redEncoderPin1 == HIGH && redEncoderPin1_old == LOW) {
      // encoder turned
//      long timeBetweenEncoderChanges = currentMillis - lastEncoderChange;

//      Serial.print("timeBetweenEncoderChanges: ");
//      Serial.println(timeBetweenEncoderChanges);
//      
//      if(timeBetweenEncoderChanges < 1000L) {
//        multi = (-9L * timeBetweenEncoderChanges) / 1000L + 10L;
//        Serial.print("multi: ");
//        Serial.println(multi);
//      }
//      
//      lastEncoderChange = currentMillis;
      
      int redEncoderPin2 = digitalRead(RED_ENCODER_PIN2);
      if(redEncoderPin2 == HIGH) {
        redEncoderValue += multi;
      } else {
        redEncoderValue -= multi;
      }
      
      redEncoderValue = constrain(redEncoderValue, 0, 200);
//      Serial.print("redEncoderValue: "); Serial.println(redEncoderValue);
//      Serial.println(redEncoderValue);
    }
    redEncoderPin1_old = redEncoderPin1;
  
    redIntensity = redEncoderValue;
    if(redIntensity != oldRedIntensity) {
      setStrip = true;
//      strip.setPixelColor(0, redIntensity, greenIntensity, blueIntensity);
//      strip.show();
      oldRedIntensity = redIntensity;
    }

    // **************************** GREEN **************************** //
    greenEncoderPin1 = digitalRead(GREEN_ENCODER_PIN1);
    if(greenEncoderPin1 == HIGH && greenEncoderPin1_old == LOW) {
      // encoder turned
      int greenEncoderPin2 = digitalRead(GREEN_ENCODER_PIN2);
      if(greenEncoderPin2 == HIGH) {
        greenEncoderValue += multi;
      } else {
        greenEncoderValue -= multi;
      }
      
      greenEncoderValue = constrain(greenEncoderValue, 0, 200);
//      Serial.println(greenEncoderValue);
    }
    greenEncoderPin1_old = greenEncoderPin1;
  
    greenIntensity = greenEncoderValue;
    if(greenIntensity != oldGreenIntensity) {
      setStrip = true;
//      strip.setPixelColor(0, redIntensity, greenIntensity, blueIntensity);
//      strip.show();
      oldGreenIntensity = greenIntensity;
    }

    // **************************** GREEN **************************** //
    blueEncoderPin1 = digitalRead(BLUE_ENCODER_PIN1);
    if(blueEncoderPin1 == HIGH && blueEncoderPin1_old == LOW) {
      // encoder turned
      int blueEncoderPin2 = digitalRead(BLUE_ENCODER_PIN2);
      if(blueEncoderPin2 == HIGH) {
        blueEncoderValue += multi;
      } else {
        blueEncoderValue -= multi;
      }
      
      blueEncoderValue = constrain(blueEncoderValue, 0, 200);
//      Serial.println(blueEncoderValue);
    }
    blueEncoderPin1_old = blueEncoderPin1;
  
    blueIntensity = blueEncoderValue;
    if(blueIntensity != oldBlueIntensity) {
      setStrip = true;
//      strip.setPixelColor(0, redIntensity, greenIntensity, blueIntensity);
//      strip.show();
      oldBlueIntensity = blueIntensity;
    }

    if(redIntensity + greenIntensity + blueIntensity < multi) {
      redIntensity = multi;
      redEncoderValue = multi;
      setStrip = true;
    }

    if(setStrip) {
      Serial.print("r: "); Serial.print(redIntensity);
      Serial.print(", g: "); Serial.print(greenIntensity);
      Serial.print(", b: "); Serial.println(blueIntensity);
      strip.setPixelColor(0, redIntensity, greenIntensity, blueIntensity);
      strip.show();
    }
  }
}






//  for(int i = 0; i <= intensity; i++) {
//      strip.setPixelColor(0, intensity, i, 0);
//      strip.show();
//      delay(del);
//    }
//  
//    for(int i = intensity; i >= 0; i--) {
//      strip.setPixelColor(0, i, intensity, 0);
//      strip.show();
//      delay(del);
//    }
//  
//    for(int i = 0; i <= intensity; i++) {
//      strip.setPixelColor(0, 0, intensity, i);
//      strip.show();
//      delay(del);
//    }
//  
//    for(int i = intensity; i >= 0; i--) {
//      strip.setPixelColor(0, 0, i, intensity);
//      strip.show();
//      delay(del);
//    }
//  
//    for(int i = 0; i <= intensity; i++) {
//      strip.setPixelColor(0, i, 0, intensity);
//      strip.show();
//      delay(del);
//    }
//  
//    for(int i = intensity; i >= 0; i--) {
//      strip.setPixelColor(0, intensity, 0, i);
//      strip.show();
//      delay(del);
//    }
//  
//    blink(0, intensity, 0, 0, 100, 10);
  
//void blink(int nmbr, int r, int g, int b, int del, int count) {
//  for(int i = 0; i < count; i++) {
//    strip.setPixelColor(0, 0, 0, 0);
//    strip.show();
//    delay(del);
//    strip.setPixelColor(0, r, g, b);
//    strip.show();
//    delay(del);
//  }
//}
